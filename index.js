fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(response => response.json())

    .then(response =>{
        response.map(element => console.log(element.title))
    })

fetch(`https://jsonplaceholder.typicode.com/todos` 
    ,{
        method: "POST",
        headers: {
            "Content-type" : "application/json"
        },
        body: JSON.stringify({
            title: "New task",
            userId: 1,
            status: "Completed"
        })
    })
    .then(response => response.json())
    .then(response =>{
     console.log(response)
})

fetch(`https://jsonplaceholder.typicode.com/todos/1` 
    ,{
        method: "PUT",
        headers: {
            "Content-type" : "application/json"
        },
        body: JSON.stringify({
            title: "Hello!",
            description: "Added description",
            userId: "1"
        })
    })
    .then(response => response.json())
    .then(response =>{
     console.log(response)
})

fetch(`https://jsonplaceholder.typicode.com/todos/2` 
    ,{
        method: "PATCH",
        headers: {
            "Content-type" : "application/json"
        },
        body: JSON.stringify({
            status: "Completed",
            date_completed: "03/07/2022",
        })
    })
    .then(response => response.json())
    .then(response =>{
     console.log(response)
})


fetch(`https://jsonplaceholder.typicode.com/todos/1`,{
    method: "DELETE",
})



